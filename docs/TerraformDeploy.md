# Terraform CI/CD Pipeline

This GitLab CI/CD pipeline template is designed for projects using Terraform for infrastructure as code (IaC) management. It supports various stages including initialization, linting (pre-commit checks), validation, planning, building, and deployment. This pipeline also integrates with SOPS for secrets management and supports dynamic environment handling.

## Requirements

- GitLab runner with Docker and Terraform installed.
- Access to a GitLab project where this pipeline will be configured.
- SOPS for encrypted Terraform variable files (`*.encrypted`).

## Pipeline Stages

1. **Init**: Initializes the Terraform working directory and backend.
2. **Pre-commit**: Runs various pre-commit hooks for linting and ensuring code quality.
3. **Validate**: Performs Terraform validation checks on the codebase.
4. **Plan**: Generates and outputs a Terraform execution plan.
5. **Build**: Applies the Terraform plan to reach the desired state of the infrastructure.
6. **Deploy**: Handles additional deployment tasks, such as Kubernetes configurations or custom scripts.

## Configuration

- `SOPS_VERSION`: Specifies the version of SOPS to be used.
- `TF_ROOT`: The root directory for Terraform files within the project.
- `TF_ADDRESS`: The Terraform Cloud or Enterprise API address for state management.
- `TF_VARS_ENCRYPTED`: The path to the encrypted Terraform variables file.
- `TF_VARS`: The path to the decrypted Terraform variables file.

## Usage

1. Ensure all requirements are met, including the installation of Terraform, Docker, and SOPS on the runner.
2. Add the `.gitlab-ci.yml` configuration to your project's root directory.
3. Customize the pipeline variables and stages as needed to fit your project.
4. Commit and push changes to your GitLab repository to trigger the pipeline.

TODO add usage code block