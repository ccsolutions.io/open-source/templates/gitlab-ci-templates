# GitLab CI Kaniko Build Template

This is a fork of the GitLab CI YAML template for building Docker images with Kaniko, originally developed by Syseleven GmbH. This forked version contains modifications and enhancements developed by CCSolutions.io UG.

The original template by Syseleven GmbH can be found [here](https://code.syseleven.de/syseleven/gitlab-ci-templates/-/blob/master/DockerBuild-kaniko.yml).

## Features

- Base Kaniko build job for customization
- Default build job for the latest image tag
- Separate build job for environment-specific images
- Job to remove development Docker images
- Support for tag-based Docker image builds

## How to Use this Template

To use this GitLab CI YAML template, you can import it in your project's `.gitlab-ci.yml` file.

Here's a basic example of how you can import and use the template:

```yaml
include:
  - project: 'csolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'DockerBuild-kaniko.yml'
    ref: 'v2.0.5'

kaniko-custom-build:
  extends: .kaniko-build-base
  variables:
    DOCKERFILE: custom.Dockerfile
    IMAGE_TAG: "custom"
```

This example extends the base Kaniko build job and sets a custom Dockerfile name and image tag.

## Variables

The template uses several predefined CI/CD variables from GitLab:

- `CI_REGISTRY`: The URL of your Docker registry.
- `CI_REGISTRY_USER`: The username to authenticate with your Docker registry.
- `CI_REGISTRY_PASSWORD`: The password to authenticate with your Docker registry.
- `CI_PROJECT_DIR`: The absolute path to your project directory in the GitLab Runner environment.
- `CI_COMMIT_REF_SLUG`: The branch or tag name for which project is built.
- `CI_COMMIT_TAG`: The commit tag name. Present only when building tags.
- `CI_PIPELINE_SOURCE`: Indicates how the pipeline was triggered.
- `CI_DEFAULT_BRANCH`: The default branch for your project.

You can set your custom variables in the `variables` section of each job or in your project settings.

## License

This project is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT).

## Authors

This project was created by [CCSolutions.io UG](mailto:devops@ccsolutions.io).

## Support

For support or any questions, please reach out to us at [devops@ccsolutions.io](mailto:devops@ccsolutions.io).

**Note:** Please refer to the official GitLab and Kaniko documentation for more information on how to use GitLab CI/CD and Kaniko.
