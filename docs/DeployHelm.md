# README

## Overview
This GitLab CI/CD pipeline template is designed for deploying Helm charts to a Kubernetes cluster. Helm charts are a collection of YAML files used to describe a set of Kubernetes resources, making them easy to manage and deploy. This pipeline includes a single stage for deploying Helm charts.

## Requirements
A GitLab account and a project repository.
A Kubernetes cluster where the Helm chart will be deployed.
The Helm CLI installed on the runner used by the CI/CD pipeline.

## Variables
- `ARGS`: Additional arguments to pass to the `helm upgrade --install` command. Default is an empty string.
- `KUBE_CONTEXT`: The Kubernetes context where the chart will be deployed.
- `RELEASE_NAME`: The name of the Helm release.
- `REPO_NAME`: The name of the Helm repository.
- `NAMESPACE`: The namespace of the kubernetes cluster
- `REPO`: The URL of the Helm repository.

## Pipeline Stage
### Deploy
This stage deploys the Helm chart to the specified Kubernetes cluster using the provided variables. It adds the Helm repository, updates the repository, and then upgrades or installs the Helm release.

## How to Use
Add this CI/CD template to your `.gitlab-ci.yml` file in your GitLab repository.
Ensure that the variables in the template match your project's requirements.
Provide values for the variables `ARGS`, `KUBE_CONTEXT`, `RELEASE_NAME`, `REPO_NAME`, `NAMESPACE`, and `REPO`.
Push your changes to GitLab, and the pipeline will automatically deploy your Helm chart to the specified Kubernetes cluster.

```yaml
---
deploy-helm:
  stage: deploy-helm
  extends: .deploy-helm
  variables:
    ARGS: "--set repository=${REPOSITORY} tag=${TAG}"
    KUBE_CONTEXT: "my-kube-context"
    RELEASE_NAME: "my-release"
    REPO_NAME: "my-repo"
    NAMESPACE: "my-namespace"
    REPO: "https://charts.example.com"
```

