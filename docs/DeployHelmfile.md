# README

## Overview
This GitLab CI/CD pipeline template is designed for deploying Helm charts using Helmfile to a Kubernetes cluster. Helmfile is a declarative specification for deploying Helm charts, making it easier to manage complex Helm deployments. The pipeline includes stages for linting, diffing, and deploying Helm charts using Helmfile.

## Requirements
A GitLab account and a project repository.
A Kubernetes cluster where the Helm charts will be deployed.
The Helmfile CLI installed on the runner used by the CI/CD pipeline.

## Variables
- `KUBE_CONTEXT`: The Kubernetes context where the Helm charts will be deployed.

## Pipeline Stages
### Lint
This stage runs linting checks on the Helmfile configuration to ensure it follows best practices and does not contain errors.

### Diff
This stage displays the difference between the current Helmfile state and the desired state, allowing you to review changes before applying them.

### Deploy
This stage applies the changes specified in the Helmfile to the Kubernetes cluster, deploying or updating Helm charts as necessary.

## How to Use
Add this CI/CD template to your `.gitlab-ci.yml` file in your GitLab repository.
Ensure that the `KUBE_CONTEXT` variable in the template matches your Kubernetes cluster's context.
Push your changes to GitLab, and the pipeline will automatically lint, diff, and deploy your Helm charts using Helmfile.

```yaml
---
variables:
    KUBE_CONTEXT: ${KUBE_CONTEXT}
    ENVIRONMENT: ${CI_ENVIRONMENT_NAME}
    
lint-helmfile:
  stage: lint
  extends: .lint-helmfile

diff-helmfile:
  stage: diff
  extends: .diff-helmfile

deploy-helmfile:
  stage: deploy
  extends: .deploy-helmfile
---
```