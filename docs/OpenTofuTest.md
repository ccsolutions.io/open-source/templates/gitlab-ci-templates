# OpenTofu Test CI/CD Pipeline

This GitLab CI/CD pipeline template is designed for projects using OpenTofu for infrastructure as code (IaC) management, focusing on pre-commit checks and testing. It supports stages for running pre-commit hooks and executing OpenTofu tests.

## Requirements

- GitLab runner with Docker and OpenTofu installed.
- Access to a GitLab project where this pipeline will be configured.

## Pipeline Stages

1. **Pre-commit**: Runs various pre-commit hooks for linting and ensuring code quality.
2. **Test**: Executes OpenTofu tests on the codebase.

## Configuration

No specific configuration variables are defined in this pipeline. However, you may need to set up environment-specific variables depending on your test requirements.

## Usage

1. Ensure all requirements are met, including the installation of OpenTofu and Docker on the runner.
2. Add the `OpenTofuTest.yml` configuration to your project's CI/CD configuration.
3. Customize the pipeline stages and pre-commit hooks as needed to fit your project.
4. Commit and push changes to your GitLab repository to trigger the pipeline.

## Pipeline Workflow

1. The pipeline starts by setting up the pre-commit configuration, including various hooks for OpenTofu and general code quality checks.
2. The pre-commit stage runs these hooks on all files in the repository.
3. The test stage executes OpenTofu tests using the `gitlab-tofu test` command.

## Pre-commit Hooks

The pipeline includes several pre-commit hooks:

- OpenTofu-specific hooks:
    - `tofu_trivy`: Scans for vulnerabilities
    - `tofu_docs`: Generates documentation
    - `tofu_tflint`: Lints OpenTofu code with various rules enabled

- General hooks:
    - `check-added-large-files`
    - `detect-private-key`
    - `trailing-whitespace`

**Note**: The `tofu_fmt` hook is commented out due to an existing issue. Check the referenced GitHub issue for updates.

This pipeline is crucial for maintaining code quality and ensuring that your OpenTofu configurations pass all necessary tests before being applied to your infrastructure.