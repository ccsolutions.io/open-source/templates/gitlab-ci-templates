# README

## Overview
This GitLab CI/CD pipeline template, `envSubs`, is designed for performing environment variable substitution in template files. It's particularly useful for dynamically generating configuration files based on environment variables, making it easier to manage different environments and configurations in your CI/CD pipeline.

## Requirements
- A GitLab account and a project repository.
- GitLab runner with Docker support.
- Alpine Linux 3.20.3 image available to the runner.
- Template files (`.tpl` files) in your repository that require environment variable substitution.

## Variables
- `DIR`: The directory containing `.tpl` files to be processed. Optional.
- `FILES`: A comma-separated list of specific files to process. Optional.
- `OUTPUT_DIR`: The directory where processed files will be saved. If not specified, files are processed in-place.

## Pipeline Stage
### Substitution
This stage performs environment variable substitution on the specified template files. It uses the `envsubst` command from the `gettext` package to replace placeholders in the template files with actual environment variable values.

## How to Use
1. Add this CI/CD template to your `.gitlab-ci.yml` file in your GitLab repository.
2. Ensure that the variables in the template match your project's requirements.
3. Provide values for the variables `DIR`, `FILES`, and `OUTPUT_DIR` as needed.
4. Push your changes to GitLab, and the pipeline will automatically perform environment variable substitution on your template files.

```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'utils/EnvSubs.yml'
    ref: 'v2.0.5'

env-substitution-job:
  extends: .env-substitution
  variables:
    DIR: "path/to/template/directory"
    FILES: "config1.yaml.tpl,config2.yaml.tpl"
    OUTPUT_DIR: "path/to/output/directory"
```

## Template Behavior
- The template will process all `.tpl` files in the specified `DIR` if provided.
- If `FILES` is specified, it will process only those files (they don't need to have a `.tpl` extension).
- Processed files will have the `.tpl` extension removed (if present).
- If `OUTPUT_DIR` is specified, processed files will be saved there. Otherwise, they will replace the original files.
- The script preserves original `.tpl` files.

## Artifacts
- Processed files (`.yaml`, `.yml`, `.env`, `.txt`) are automatically saved as job artifacts.
- Artifacts are set to expire after 1 week by default.
- The `.gitlab-ci.yml` file is excluded from artifacts.

## Notes
- Ensure all necessary environment variables are properly set in your GitLab CI/CD settings or `.gitlab-ci.yml` file.
- The template uses Alpine Linux 3.20.3 and installs the `gettext` package for `envsubst` command.
- For troubleshooting and more detailed information, refer to the job logs in your GitLab CI/CD pipeline.