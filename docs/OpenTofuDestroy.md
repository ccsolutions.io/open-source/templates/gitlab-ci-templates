# OpenTofu Destroy CI/CD Pipeline

This GitLab CI/CD pipeline template is designed for projects using OpenTofu (an open-source fork of Terraform) for infrastructure as code (IaC) management, specifically for destroying resources. It supports the destruction stage and integrates with SOPS for secrets management.

## Requirements

- GitLab runner with Docker and OpenTofu installed.
- Access to a GitLab project where this pipeline will be configured.
- SOPS for encrypted OpenTofu variable files (`*.encrypted`).
- GPG key for decryption.

## Pipeline Stages

1. **Destroy**: Destroys the infrastructure defined by the OpenTofu configuration.

## Configuration

- `SOPS_VERSION`: Specifies the version of SOPS to be used.
- `TOFU_VARS_ENCRYPTED`: The path to the encrypted OpenTofu variables file.
- `TOFU_VARS`: The path to the decrypted OpenTofu variables file.
- `GPG_KEY`: The GPG key used for decryption.
- `PASSPHRASE`: The passphrase for the GPG key.

## Usage

1. Ensure all requirements are met, including the installation of OpenTofu, Docker, and SOPS on the runner.
2. Add the `OpenTofuDestroy.yml` configuration to your project's CI/CD configuration.
3. Set up the required variables in your GitLab CI/CD settings.
4. Customize the pipeline variables and stages as needed to fit your project.
5. Trigger the pipeline manually when you want to destroy your infrastructure.

## Pipeline Workflow

1. The pipeline starts by setting up the environment, installing necessary tools like curl, gnupg, and SOPS.
2. It imports the GPG key and sets up the passphrase for decryption.
3. The destroy stage decrypts the OpenTofu variables file and runs the `gitlab-tofu destroy` command to tear down the infrastructure.

**Note**: Be cautious when using this pipeline as it will destroy your infrastructure. Always double-check before triggering the destroy pipeline.