# README

## Overview
This GitLab CI/CD pipeline template is designed for deploying applications to a Kubernetes Cluster.\
This template aims to improve the deployment process using Infrastructure as Service (IaC).\
The template is a combination between our [DockerBuild-Kaniko](DockerBuild-kaniko.md) and [DeployHelm](DeployHelmfile.md) templates.

## Requirements
A GitLab account and a project repository.\
A Kubernetes cluster where the complete infrastructure will be deployed.

## Variables

The necessary variable are the same as the [DockerBuild-Kaniko](DockerBuild-kaniko.md) and [DeployHelm](DeployHelmfile.md) templates.

With the addition of the following variables:

 - `HOST_URL`: The URL of the application used by the Ingress.
 - `SECRET_NAME`: Secret Name for the TLS certificate.

## Pipeline Stages

### Build
This stage runs the Docker build process using Kaniko, building the Docker image for the application.

### Deploy
This stage aims to Deploy the Helm charts to the Kubernetes cluster using the specified configuration. 

### Delete
This stage aims to delete the namespace from the Kubernetes cluster.

## How to Use
Add this CI/CD template to your `.gitlab-ci.yml` file in your GitLab repository.\
Ensure that the `KUBE_CONTEXT` variable in the template matches your Kubernetes cluster's context.\
Push your changes to GitLab, and the pipeline will automatically build and deploy your application to the specified Kubernetes cluster.
In addition, you can configure your pipeline using this template in case you need different behaviors.

In the following example, we are using the template to build and deploy an application to a Kubernetes cluster in two environments `development` and `staging`.

This allows us to deploy the application to the development environment when a feature branch is created 
and to the staging environment when a merge request is created and is not a draft.

```yaml
---

stages:
  - build
  - deploy
  - delete

default:
  tags:
    - gitlab-org-docker

variables:
  DOMAIN_NAME: <your_domain_name>
  GLOBAL_KUBE_CONTEXT: <your_kube_context>
  GLOBAL_RELEASE_NAME: <your_release_name>
  GLOBAL_APPLICATION_NAME: <your_application_name>
  GLOBAL_REPO_NAME: <your_repo_name>
  GLOBAL_CHART_NAME: <your_chart_name>
  GLOBAL_REPO_URL: <your_repo_url>
  INGRESS_PATH: "/"
  INGRESS_PATHTYPE: "Prefix"
  DEVELOPMENT_IMAGE_TAG: ${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
  DEVELOPMENT_NAMESPACE: ${GLOBAL_RELEASE_NAME}-${CI_COMMIT_REF_SLUG}
  STAGING_IMAGE_TAG: staging-${CI_COMMIT_SHORT_SHA}

include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'DeploymentPipeline/DeploymentPipeline.yml'
    ref: 'v2.0.5'

# Template for build-image jobs
.build-image-template:
  stage: build
  extends: .build-image
  variables:
    IMAGE_TAG: ${DEVELOPMENT_IMAGE_TAG}
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^(feature|hotfix|bug)\/.*$/ && $CI_OPEN_MERGE_REQUESTS == null'
      when: always
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: always

# Template for deploy jobs
.deploy-template:
  stage: deploy
  extends: .deploy-helm
  before_script:
    - cp "${VALUES}" values.yml
  variables:
    KUBE_CONTEXT: ${GLOBAL_KUBE_CONTEXT}
    RELEASE_NAME: ${GLOBAL_RELEASE_NAME}-${GLOBAL_APPLICATION_NAME}
    APPLICATION_NAME: ${GLOBAL_APPLICATION_NAME}
    CHART_NAME: ${GLOBAL_CHART_NAME}
    REPO_URL: ${GLOBAL_REPO_URL}
    REPO_NAME: ${GLOBAL_REPO_NAME}
    SECRET_NAME: ${HOST_URL}-tls
    ARGS: "-f values.yml --set fullnameOverride=${GLOBAL_APPLICATION_NAME}
          --set image.repository=${CI_REGISTRY_IMAGE} --set image.tag=${IMAGE_TAG}
          --set registryCredentials.username=${REGISTRY_USERNAME} 
          --set registryCredentials.password=${REGISTRY_PASSWORD}
          --set ingress.hosts[0].host=${HOST_URL} --set ingress.hosts[0].paths[0].path=${INGRESS_PATH} 
          --set ingress.hosts[0].paths[0].pathType=${INGRESS_PATHTYPE} --set ingress.tls[0].secretName=${SECRET_NAME} 
          --set ingress.tls[0].hosts[0]=${HOST_URL}"

# Jobs
build-image:development:
  extends:
    - .build-image-template

build-image:staging:
  extends: .build-image-template
  environment:
    name: staging
  variables:
    IMAGE_TAG: ${STAGING_IMAGE_TAG}
  rules:
    - if: $CI_COMMIT_BRANCH == "main"

kaniko-build-tags:
  extends: .build-image-template
  environment:
    name: production
  variables:
    IMAGE_TAG: ${CI_COMMIT_TAG}
  rules:
    - if: $CI_COMMIT_TAG

deploy:development:
  needs: [build-image:development]
  extends:
    - .deploy-template
  environment:
    name: ${CI_COMMIT_REF_SLUG}
    url: https://${HOST_URL}
  variables:
    IMAGE_TAG: ${DEVELOPMENT_IMAGE_TAG}
    NAMESPACE: ${DEVELOPMENT_NAMESPACE}
    HOST_URL: ${CI_COMMIT_REF_SLUG}.${DOMAIN_NAME}
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^(feature|hotfix|bug)\/.*$/ && $CI_OPEN_MERGE_REQUESTS == null'
      when: manual


deploy:staging:
  needs: [build-image:staging]
  extends: .deploy-template
  environment:
    name: staging
    url: https://${HOST_URL}
  variables:
    IMAGE_TAG: ${STAGING_IMAGE_TAG}
    NAMESPACE: ${GLOBAL_RELEASE_NAME}-${CI_ENVIRONMENT_NAME}
    HOST_URL: staging.${DOMAIN_NAME}
  rules:
    - if: $CI_COMMIT_BRANCH == "main"

deploy:production:
  needs: [kaniko-build-tags]
  extends: .deploy-template
  environment:
    name: production
    url: https://${HOST_URL}
  variables:
    IMAGE_TAG: ${CI_COMMIT_TAG}
    NAMESPACE: ${GLOBAL_RELEASE_NAME}-${CI_ENVIRONMENT_NAME}
    HOST_URL: ${DOMAIN_NAME}
  rules:
    - if: $CI_COMMIT_TAG

delete-environment:
  needs: [deploy:staging]
  stage: delete
  extends: .delete-environment
  variables:
    KUBE_CONTEXT: ${GLOBAL_KUBE_CONTEXT}
  rules:
    - if: $CI_COMMIT_BRANCH == "main"

```