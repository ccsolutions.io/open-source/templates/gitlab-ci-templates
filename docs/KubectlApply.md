# README

## Overview
This GitLab CI/CD pipeline template is designed for deploying kubernetes Resources using kubectl CLI in a Kubernetes cluster. 

## Requirements
A GitLab account and a project repository.
A Kubernetes cluster where the Resources will be deployed.
The kubectl CLI installed on the runner used by the CI/CD pipeline.

## Variables
- `KUBE_CONTEXT`: The Kubernetes context where the Helm charts will be deployed.
- `RESOURCE_PATH`: Path to the kubernetes resources to deploy
- `NAMESPACE`: Namespace in which the kubernetes resourse is deployed
- `KUBE_OPT`: For other CLI options, example: --dry-run


## How to Use
Add this CI/CD template to your `.gitlab-ci.yml` file in your GitLab repository.
Ensure that the `KUBE_CONTEXT` variable in the template matches your Kubernetes cluster's context.
Push your changes to GitLab, and the pipeline will automatically apply the kubernetes file into the cluster

## Example
```yaml
---
deploy:kubectl-apply:
  extends: .kubectl-apply
  variables:
    RESOURCE_PATH: ./testDeploy.yaml
    KUBECTL_OPT: --dry-run=none
    KUBE_CONTEXT: "Context-path"
    NAMESPACE: default

---
```