# README
## Overview
This GitLab CI/CD pipeline template is designed for running Ansible playbooks. Ansible is a powerful IT automation tool that can configure systems, deploy software, and orchestrate more advanced IT tasks. This pipeline includes two stages: one for checking the playbook syntax and another for executing the playbook.

## Requirements
A GitLab account and a project repository.
An Ansible playbook, inventory file, and secrets file.
The willhallonline/ansible Docker image available to the runner used by the CI/CD pipeline.
A vault password file for decrypting Ansible Vault secrets.

## Variables
`ANSIBLE_DIR`: The directory where the Ansible files are located. Default is ".".
`PLAYBOOK`: The name of the playbook file to run. Default is "playbook.yaml".
`INVENTORY`: The name of the inventory file. Default is "inventory.yaml".
`SECRETS`: The name of the secrets file. Default is "secrets.yaml".
`VAULT_PASSWORD_FILE`: The path to the vault password file. Default is ".vault.secret".
`ANSIBLE_VERSION`: The version of the Ansible Docker image to use. Default is "2.15-alpine-3.18".

## Pipeline Stages

### Check

This stage checks the syntax and potential changes of the Ansible playbook without applying them. It uses the --check flag to perform a dry run.

```yaml
.ansible_check:
  image:
    name: "willhallonline/ansible:${ANSIBLE_VERSION}"
    entrypoint: [""]
  stage: check
  script:
    - ansible --version
    - cp ${ANSIBLE_VAULT_PASSWORD_FILE} .vault.secret
    - ansible-playbook -i ${ANSIBLE_DIR}/${INVENTORY} ${PLAYBOOK} -e @./${SECRETS} --vault-password-file ${ANSIBLE_DIR}/.vault.secret --check
```
### Run

This stage runs the Ansible playbook to apply the desired configurations to the target systems.

```yaml
.ansible_run:
  image:
    name: "willhallonline/ansible:${ANSIBLE_VERSION}"
    entrypoint: [""]
  stage: run
  script:
    - ansible --version
    - cp ${ANSIBLE_VAULT_PASSWORD_FILE} .vault.secret
    - ansible-playbook -i ${ANSIBLE_DIR}/${INVENTORY} ${PLAYBOOK} -e @./${SECRETS} --vault-password-file ${ANSIBLE_DIR}/.vault.secret
```
# How to Use
Add this CI/CD template to your .gitlab-ci.yml file in your GitLab repository.
Ensure that the variables in the template match your project's requirements.
Provide values for the variables ANSIBLE_DIR, PLAYBOOK, INVENTORY, SECRETS, VAULT_PASSWORD_FILE, and ANSIBLE_VERSION.
Push your changes to GitLab, and the pipeline will automatically check the syntax and run your Ansible playbook based on the specified stages.

```yaml
include:
  - project: 'csolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'ansibleRun/AnsibleRun.yml'
    ref: 'v2.0.5'

ansible-check:
  extends: .ansible_check
  variables:
    ANSIBLE_DIR: ""
    PLAYBOOK: ""
    INVENTORY: ""
    SECRETS: ""
    VAULT_PASSWORD_FILE: ""
    ANSIBLE_VERSION: ""

ansible-run:
  extends: .ansible_run
  variables:
    ANSIBLE_DIR: ""
    PLAYBOOK: ""
    INVENTORY: ""
    SECRETS: ""
    VAULT_PASSWORD_FILE: ""
    ANSIBLE_VERSION: ""

```