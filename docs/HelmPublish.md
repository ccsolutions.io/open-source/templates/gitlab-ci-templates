# README

## Overview
This GitLab CI/CD pipeline template is designed for automating the packaging and publishing of Helm charts. Helm charts are a collection of YAML files used to describe a set of Kubernetes resources and are an essential tool for Kubernetes application management. The pipeline is divided into two main stages: packaging (package) and publishing (publish).

## Requirements
A GitLab account and a project repository.
The Helm CLI installed on the runner used by the CI/CD pipeline.
Access to a Kubernetes cluster where the Helm chart will be deployed (optional for this pipeline, but necessary for deploying the chart).

## Variables
- CHART_PATH: The path to the Helm chart in your repository. Default is set to ./.
- CHANNEL: The Helm chart repository channel where the chart will be published. Default is stable.

## Pipeline Stages
### Packaging
This stage prepares the Helm chart for publishing by updating the chart version with the Git tag associated with the commit, validating the chart (linting), updating dependencies, and packaging it.

### Publishing
This stage uploads the packaged Helm chart to the specified Helm repository within GitLab using a POST request.

## How to Use
Add this CI/CD template to your .gitlab-ci.yml in your GitLab repository.
Ensure that the CHART_PATH and CHANNEL variables in the template match your project's requirements.
Tag your commits with the version of the Helm chart you want to publish.
Push your changes to GitLab, and the pipeline will automatically handle the packaging and publishing of your Helm chart based on the tags.

```yaml
---

stages:
  - package
  - publish


default:
  tags:
    - gitlab-org-docker
  variables:
    CHART_PATH: "./" 
    CHANNEL: "stable"


include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'HelmPipelines/HelmPublish.yml'
    ref: 'v2.0.5'


package:
  extends: .package_chart

publish:
  extends: .publish_chart

```
