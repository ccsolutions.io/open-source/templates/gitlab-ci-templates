# OpenTofu CI/CD Pipeline

This GitLab CI/CD pipeline template is designed for projects using OpenTofu for infrastructure as code (IaC) management. It supports various stages including pre-commit checks, initialization, planning, and applying changes. This pipeline also integrates with SOPS for secrets management.

## Requirements

- GitLab runner with Docker and OpenTofu installed.
- Access to a GitLab project where this pipeline will be configured.
- SOPS for encrypted OpenTofu variable files (`*.encrypted`).
- GPG key for decryption.

## Pipeline Stages

1. **Pre-commit**: Runs various pre-commit hooks for linting and ensuring code quality.
2. **Init**: Initializes the OpenTofu working directory and backend.
3. **Plan**: Generates and outputs an OpenTofu execution plan.
4. **Apply**: Applies the OpenTofu plan to reach the desired state of the infrastructure.

## Configuration

- `SOPS_VERSION`: Specifies the version of SOPS to be used.
- `TF_ROOT`: The root directory for OpenTofu files within the project.
- `TF_ADDRESS`: The OpenTofu state management API address.
- `TOFU_VARS_ENCRYPTED`: The path to the encrypted OpenTofu variables file.
- `TOFU_VARS`: The path to the decrypted OpenTofu variables file.
- `GPG_KEY`: The GPG key used for decryption.
- `PASSPHRASE`: The passphrase for the GPG key.

## Usage

1. Ensure all requirements are met, including the installation of OpenTofu, Docker, and SOPS on the runner.
2. Add the `OpenTofu.yml` configuration to your project's CI/CD configuration.
3. Set up the required variables in your GitLab CI/CD settings.
4. Customize the pipeline variables and stages as needed to fit your project.
5. Commit and push changes to your GitLab repository to trigger the pipeline.

## Pipeline Workflow

1. The pipeline starts by setting up the environment, installing necessary tools like curl, gnupg, and SOPS.
2. It imports the GPG key and sets up the passphrase for decryption.
3. The pre-commit stage runs various checks including OpenTofu-specific linters and general code quality tools.
4. The init stage initializes the OpenTofu working directory.
5. The plan stage generates an execution plan, which is saved as an artifact.
6. The apply stage executes the plan to create or modify the infrastructure.

**Note**: This pipeline uses the `gitlab-tofu` wrapper around OpenTofu, which is designed to work seamlessly with GitLab CI/CD.