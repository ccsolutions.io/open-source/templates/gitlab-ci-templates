# Terraform GitLab CI/CD Pipeline

## How to use:

There are two templates:
- TerraformDeploy.yml 
- TerraformDestroy.yml

They must be referenced in the main ci file with "include:". See ExamplePipeline.gitlab-ci.yml for an example.

In this example's case, if you want to destroy instead of deploy, uncomment the destroy step and comment everything else in the ExamplePipeline.gitlab-ci.yml file. All values and steps in the main file overwrite the template if present.

## Note

The deployment step is Kubernetes-specific. In the future, we might decide to remove the Kubernetes-specific part and create a standalone template for the deploy step.
