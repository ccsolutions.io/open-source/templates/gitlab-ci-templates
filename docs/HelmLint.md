# Helm Lint GitLab CI/CD Pipeline

This is about a GitLab CI/CD pipeline configuration (`gitlab-ci.yaml`) that runs Helm Lint on your Helm charts. Helm Lint is a tool that checks for common issues in your Helm charts and provides suggestions for improvements.

## Pipeline Stages

The pipeline consists of a single stage: `lint`.

## Jobs

The pipeline contains one job: `helm-lint`.

### helm-lint

This job runs Helm Lint on your Helm charts. It uses the `alpine/helm:3.13.3` Docker image and the following environment variables:

- `GIT_DEPTH`: The number of commits to fetch from the Git repository. Set to `"1"` to fetch only the latest commit.
- `HELM_VERSION`: The version of Helm to use. Set to `latest` to use the latest version.
- `HELM_PATH`: The path to the Helm charts to lint. Leave blank to lint all charts in the repository.
- `HELM_ADDITIONAL_ARGS`: Any additional arguments to pass to the `helm lint` command.
- `HELM_LINT_STRICT`: Set to `'--strict'` to enable strict linting, which treats warnings as errors.

The job runs on merge request events and on commits to the default branch.

## How to Use this Template

To use this GitLab CI YAML template, you can import it in your project's `.gitlab-ci.yml` file.

Here's a basic example of how you can import and use the template:

```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'HelmLint.yml'
    ref: 'v2.0.5'

kaniko-custom-build:
  extends: .kaniko-build-base
  variables:
    DOCKERFILE: custom.Dockerfile
    IMAGE_TAG: "custom"
```