# Runbook

## Introduction
This runbook provides operational guidance for using the GitLab CI/CD pipeline to build Docker images, deploy Helm charts, and delete environments. It outlines the steps necessary to prepare for execution, monitor the pipeline, troubleshoot common issues, and manually handle deployments or rollbacks if needed.

## Pre-execution Checklist
- Ensure that the Dockerfile, Helm chart, and any required configuration files are available and up-to-date in the project repository.
- Check that the Docker image registry and Helm repository specified in the pipeline are accessible to the CI/CD runner.
- Verify that the Kubernetes context and namespace configurations are correctly set up for the deployment and deletion stages.

## Execution Steps
### Triggering the Pipeline
The pipeline is triggered automatically on each push to the repository. Ensure that the `.gitlab-ci.yml` file includes the defined stages and tasks.

```yaml
stages:
  - build
  - deploy
  - delete

default:
  tags:
    - gitlab-org-docker

include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'DockerBuild-kaniko.yml'
    ref: 'v2.0.5'
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'HelmPipelines/DeployHelm.yml'
    ref: 'v2.0.5'

.build-image:
  stage: build
  extends: .kaniko-build-base
  variables:
    DOCKERFILE: Dockerfile
    IMAGE_TAG: ""

.helm-deploy:
  stage: deploy
  extends: .deploy-helm
  variables:
    IMAGE_TAG: ""
    NAMESPACE: ""
    KUBE_CONTEXT: ""
    RELEASE_NAME: ""
    APPLICATION_NAME: ""
    CHART_NAME: ""
    REPO_URL: ""
    REPO_NAME: ""
    HOST_URL: ""
    SECRET_NAME: ""
    ARGS: ""

.delete-environment:
  stage: delete
  image:
    name: ghcr.io/helmfile/helmfile:v0.163.1
    entrypoint: ['']
  allow_failure: true
  before_script:
    - |
      # Get the name of the branch that was just merged
      MERGED_BRANCH=$(git log --merges -n 1 --pretty=format:"%s" | sed -n 's/^Merge branch '"'"'\(.*\)'"'"' into.*$/\1/p')

      if [ -n "$MERGED_BRANCH" ]; then
        # Transform the branch name
        # Replace '#' with '--', '/' with '-', and remove any resulting '---'
        TRANSFORMED_BRANCH=$(echo "$MERGED_BRANCH" | sed 's|#|--|g; s|/|-|g; s|---|--|g')

        # Construct the namespace name based on the transformed merged branch
        export NAMESPACE="${GLOBAL_RELEASE_NAME}-${TRANSFORMED_BRANCH}"
        echo "Namespace to delete: $NAMESPACE"
      else
        echo "Could not determine the merged branch. No namespace deletion will be performed."
        exit 1
      fi
  variables:
    KUBE_CONTEXT: ""
    NAMESPACE: ""
  script:
    - kubectl config use-context ${KUBE_CONTEXT}
    - helm uninstall --kube-context ${KUBE_CONTEXT} -n ${NAMESPACE} $(helm ls --kube-context ${KUBE_CONTEXT} --short -n ${NAMESPACE})
    - kubectl delete all --all -n ${NAMESPACE}
    - kubectl delete namespace ${NAMESPACE}  
```

### Build Stage

 - The .build-image job uses Kaniko to build a Docker image from the specified Dockerfile.
 - Ensure that the DOCKERFILE and IMAGE_TAG variables are correctly set.

### Deploy Stage

 - The .helm-deploy job deploys the Helm chart to the specified Kubernetes context and namespace.
 - Ensure that the variables such as IMAGE_TAG, NAMESPACE, KUBE_CONTEXT, RELEASE_NAME, APPLICATION_NAME, CHART_NAME, REPO_URL, REPO_NAME, HOST_URL, SECRET_NAME, and ARGS are correctly set.

### Delete Stage

 - The .helm-deploy job deploys the Helm chart to the specified Kubernetes context and namespace.
 - Ensure that the variables such as IMAGE_TAG, NAMESPACE, KUBE_CONTEXT, RELEASE_NAME, APPLICATION_NAME, CHART_NAME, REPO_URL, REPO_NAME, HOST_URL, SECRET_NAME, and ARGS are correctly set.

## Monitoring

Use GitLab's CI/CD pipeline view to monitor the progress of the build, deploy, and delete stages. Check the console output for any errors or warnings.

## Troubleshooting

### Check Stage Output
 - Examine the output logs of the pipeline stages for any error messages or warnings.
 - Ensure that the Dockerfile, Helm chart, and Kubernetes configurations are correct and that all required files and variables are properly configured.

### Docker Build Errors
 - If the build stage fails, review the Kaniko error messages to identify the cause of the failure. Common issues include incorrect Dockerfile syntax, missing dependencies, or registry access problems.

### Helm Deployment Errors
- If the deploy stage fails, review the Helm error messages to identify the cause of the failure. Common issues include incorrect Helm chart configurations, unreachable Kubernetes clusters, or missing secrets.

### Kubernetes Deletion Errors
 - If the delete stage fails, review the kubectl error messages to identify the cause of the failure. Common issues include incorrect Kubernetes context or namespace settings.

## Rollback/Recovery

### Revert Changes
 - If the deployment introduced unwanted changes, revert the changes in the source code repository and commit the reversal.
### Manual Intervention
 - In cases where manual intervention is required to fix issues, access the affected Kubernetes cluster directly and apply necessary corrections following standard operating procedures.
### Redeploy
 - After addressing any issues, re-run the pipeline to redeploy the application or reapply the necessary changes. Ensure that the pipeline stages complete successfu