# OpenTofu Main Pipeline Runbook

## Introduction

This runbook provides operational procedures for the main OpenTofu CI/CD pipeline, including setup instructions, execution details, troubleshooting tips, and rollback procedures.

## Setup

Before executing the pipeline, ensure:

1. All software dependencies are installed on the runner (OpenTofu, Docker, SOPS, curl, gnupg).
2. The GitLab project is configured with the necessary environment variables (`GPG_KEY`, `PASSPHRASE`, `TF_ADDRESS`).
3. The encrypted variables file (`terraform.tfvars.encrypted`) is updated and available in the project repository.

## Execution

1. **Initiate the Pipeline**: The pipeline runs automatically upon pushing code changes to the repository.
2. **Monitor Progress**: Use GitLab's CI/CD interface to track the pipeline's progress across stages (pre-commit, init, plan, apply).

## Troubleshooting

- **Pre-commit Failures**: Check the output for specific linting errors and adjust the code accordingly.
- **OpenTofu Init Errors**: Verify that the backend configuration is correct and that the runner has the necessary permissions to access the state storage.
- **OpenTofu Plan/Apply Errors**: Review OpenTofu's output logs for details on the failure. Common issues include syntax errors, missing resources, or misconfigured providers.

## Rollback

If an OpenTofu apply introduces an undesired state:

1. Use the OpenTofu CLI to inspect the current state and identify the changes to revert.
2. Prepare a new set of OpenTofu configurations that represent the desired state prior to the faulty apply.
3. Commit and push the rollback configurations to trigger the pipeline and restore the previous state.

## State Management

- Regularly back up the OpenTofu state file.
- If using remote state, ensure that state locking is properly configured to prevent concurrent modifications.

## Security Considerations

- Rotate the `GPG_KEY` and `PASSPHRASE` periodically.
- Regularly audit the permissions granted to the CI/CD pipeline in your cloud environments.

## Support

If you encounter any issues while following this runbook, or if you have any questions or require support, please reach out to [devops@ccsolutions.io](mailto:devops@ccsolutions.io).