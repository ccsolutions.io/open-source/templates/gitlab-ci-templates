# Runbook

## Introduction
This runbook provides operational guidance for using the GitLab CI/CD pipeline to automate the deployment of Helm charts to a Kubernetes cluster using Helmfile. It outlines the steps necessary to prepare for execution, troubleshoot common issues, and manually rollback or delete published charts if needed.


## Pre-execution Checklist
- Verify that the Helm CLI is installed and configured correctly on the runner.
- Check that there is a kube config that can be used
- Ensure that your GitLab project has a tagged commit that specifies the chart version you wish to publish.

## Execution Steps
- Triggering the Pipeline: The pipeline is triggered automatically on the DeployHelmfile in the repository. Ensure that the name file matches the name in the pipeline diferent stages. lint, diff or deploy.
```yaml
lint-helmfile:
  stage: lint
  extends: .lint-helmfile

```
- Monitoring: Use GitLab's CI/CD pipeline view to monitor the progress of the packaging and publishing stages.

## Troubleshooting
- Package Stage Fails: Check the output logs for errors related to Helm linting or dependency issues. Ensure all chart dependencies are correct and accessible.


## Rollback/Deletion
If you need to rollback or delete a published chart:

1. Access your Helm repository through the GitLab UI or API.
2. Locate the chart version you wish to remove.
3. Use the repository's management tools to delete or rollback the chart