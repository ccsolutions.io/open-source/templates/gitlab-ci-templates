# Helm Lint GitLab CI/CD Pipeline Runbook

This runbook provides instructions for managing and troubleshooting the Helm Lint GitLab CI/CD pipeline.

## Managing the Pipeline

To manage the pipeline:

1. Navigate to the GitLab project.
2. Click on "CI/CD" in the left-hand menu.
3. Click on "Pipelines".

Here, you can view the status of recent pipelines, manually run a pipeline, and cancel running pipelines.

## Troubleshooting the Pipeline

If the `helm-lint` job fails:

1. Click on the failed job in the pipeline view to see the job's log output.
2. Look for any error messages in the log output. These messages should provide clues as to why the job failed.
3. If the error message indicates a problem with a Helm chart, check the chart for issues. Use the suggestions provided by Helm Lint to improve the chart.
4. If the error message indicates a problem with the `helm lint` command itself, check the `HELM_PATH` and `HELM_ADDITIONAL_ARGS` environment variables for issues.
5. If the error message indicates a problem with fetching the Git repository, check the `GIT_DEPTH` environment variable.

If you cannot resolve the issue, contact the repository owner or a knowledgeable colleague for assistance.

## Support
If you encounter any issues while following this runbook, or if you have any questions or require support, please reach out to [devops@ccsolutions.io](mailto:devops@ccsolutions.io).
