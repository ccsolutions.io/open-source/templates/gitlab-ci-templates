# Runbook

## Introduction
This runbook provides operational guidance for using the GitLab CI/CD pipeline to deploy kubernetes Resources using kubectl CLI. It outlines the steps necessary to prepare for execution, troubleshoot common issues, and manually rollback or delete kubernetes resources if needed.

## Pre-execution Checklist
- Verify that the Kubectl CLI is installed and configured correctly on the runner.
- Check that all resource files are defined and available.


## Execution Steps
- Triggering the Pipeline: The pipeline is triggered automatically on the kubernetes resource file in the repository. Ensure that the name file matches the name in the pipeline.
```yaml
deploy:kubectl-apply:
  stage: deploy
  extends: .kubectl-apply

```

- Monitoring: Use GitLab's CI/CD pipeline view to monitor the progress of deploy stage.

## Troubleshooting
- Deploy Stage Fails: Check the output logs for errors related to kubectl apply fails 


## Rollback/Deletion
If you need to rollback or delete a published chart:

1. Access your pod, svc or kubernetes resource through the GitLab UI or API.
2. Locate the resource you wish to remove.
3. Use the repository's management tools to delete or rollback the resource