# OpenTofu Destroy Pipeline Runbook

## Introduction

This runbook provides operational procedures for the OpenTofu Destroy CI/CD pipeline, including setup instructions, execution details, troubleshooting tips, and safety precautions.

## Setup

Before executing the pipeline, ensure:

1. All software dependencies are installed on the runner (OpenTofu, Docker, SOPS, curl, gnupg).
2. The GitLab project is configured with the necessary environment variables (`GPG_KEY`, `PASSPHRASE`).
3. The encrypted variables file (`terraform.tfvars.encrypted`) is updated and available in the project repository.

## Execution

1. **Initiate the Pipeline**: The destroy pipeline should be manually triggered, as it's a destructive operation.
2. **Monitor Progress**: Use GitLab's CI/CD interface to track the pipeline's progress through the destroy stage.

## Troubleshooting

- **Decryption Errors**: Verify that the `GPG_KEY` and `PASSPHRASE` are correctly set in the GitLab CI/CD variables.
- **OpenTofu Destroy Failures**: Review the OpenTofu output logs for details on the failure. Common issues include:
    - Resources that have been manually modified outside of OpenTofu
    - Dependencies between resources that prevent destruction
    - Insufficient permissions to destroy certain resources

## Safety Precautions

1. Always double-check that you're running the destroy pipeline in the correct environment.
2. Consider implementing a manual approval step before the destroy operation begins.
3. Ensure you have recent backups of any critical data before proceeding with destruction.

## Post-Destruction Verification

After the destroy pipeline completes:

1. Use the OpenTofu CLI to run `terraform show` and verify that no resources remain in the state.
2. Check your cloud provider's console to ensure all resources have been properly removed.

## Support

If you encounter any issues while following this runbook, or if you have any questions or require support, please reach out to [devops@ccsolutions.io](mailto:devops@ccsolutions.io).