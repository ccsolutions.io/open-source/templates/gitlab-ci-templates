# GitLab CI Kaniko Build Template Runbook

This runbook provides step-by-step instructions for using the GitLab CI Kaniko Build Template.

## Step 1: Importing the Template

1. Navigate to your GitLab project and open your project's `.gitlab-ci.yml` file. If the file doesn't exist, create one.
2. Import the Kaniko Build Template into your `.gitlab-ci.yml` file. Replace `your-group`, `your-project`, and `path-to/kaniko-template.yml` with the actual group, project, and file path to the Kaniko template.

```yaml
include:
  - project: 'https://gitlab.com/ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'DockerBuild-kaniko.yml'
    ref: 'v2.0.5'
```

## Step 2: Extending the Template

You can use the provided Kaniko build jobs as base jobs for your own CI/CD jobs. Extend the necessary jobs and provide any required variables.

```yaml
kaniko-custom-build:
  extends: .kaniko-build-base
  variables:
    DOCKERFILE: custom.Dockerfile
    IMAGE_TAG: "custom"
```

In this example, a custom job called `kaniko-custom-build` is created by extending the `.kaniko-build-base` job. The job uses a Dockerfile called `custom.Dockerfile` and tags the resulting Docker image with `custom`.

## Step 3: Running the Pipeline

After you have set up your `.gitlab-ci.yml` file, commit and push the changes to your GitLab repository. The CI/CD pipeline will start automatically.

## Step 4: Monitoring the Pipeline

You can monitor the pipeline's progress on the CI/CD dashboard in your GitLab project. The pipeline's status will be displayed, along with a detailed log of each job.

## Step 5: Verifying the Docker Image

Once the pipeline has completed successfully, verify that the Docker image has been created and pushed to your Docker registry. You can do this by checking the registry or by pulling the image.

## Support

If you encounter any issues while following this runbook, or if you have any questions or require support, please reach out to [devops@ccsolutions.io](mailto:devops@ccsolutions.io).
