# Runbook

## Introduction
This runbook provides operational guidance for using the GitLab CI/CD pipeline to deploy Helm charts. It outlines the steps necessary to prepare for execution, troubleshoot common issues, and manually rollback or delete published charts if needed.

## Pre-execution Checklist
- Verify that the Helm CLI is installed and configured correctly on the runner.
- Check that all Helm chart dependencies are defined and available.
- Ensure that your GitLab project has a tagged commit that specifies the chart version you wish to publish.

## Execution Steps
- Triggering the Pipeline: The pipeline is triggered automatically on the DeployHelm file in the repository. Ensure that the name file matches the name in the pipeline.
```yaml
deploy-helm:
  stage: deploy-helm
  extends: .deploy-helm

```
- Monitoring: Use GitLab's CI/CD pipeline view to monitor the progress of the packaging and publishing stages.

## Troubleshooting
- Package Stage Fails: Check the output logs for errors related to Helm linting or dependency issues. Ensure all chart dependencies are correct and accessible.


## Rollback/Deletion
If you need to rollback or delete a published chart:

1. Access your Helm repository through the GitLab UI or API.
2. Locate the chart version you wish to remove.
3. Use the repository's management tools to delete or rollback the chart