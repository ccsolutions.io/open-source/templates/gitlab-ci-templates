# Runbook for envSubs Template

## Introduction
This runbook provides operational guidance for using the `envSubs` GitLab CI/CD template. This template is designed to perform environment variable substitution in template files, making it useful for dynamically generating configuration files based on environment variables.

## Pre-execution Checklist
- Ensure that the GitLab runner is using the specified Alpine image (version 3.20.3).
- Verify that the `gettext` package is available and can be installed on the runner.
- Check that all necessary environment variables are properly set in your GitLab CI/CD settings or `.gitlab-ci.yml` file.
- Confirm that the template files (`.tpl` files) you want to process are present in the repository.

## Execution Steps
1. **Template Configuration**: Include the `envSubs` template in your `.gitlab-ci.yml` file:
   ```yaml
   include:
      - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
        file: 'utils/EnvSubs.yml'
        ref: 'v2.0.5'

   your-job-name:
     extends: .env-substitution
     variables:
       DIR: "path/to/template/directory"
       FILES: "file1.yaml.tpl,file2.yaml.tpl"
       OUTPUT_DIR: "path/to/output/directory"
   ```

2. **Variable Setup**: Set the following variables as needed:
    - `DIR`: Directory containing `.tpl` files (optional)
    - `FILES`: Comma-separated list of specific files to process (optional)
    - `OUTPUT_DIR`: Directory to save processed files (optional)

3. **Triggering the Pipeline**: The job will run as part of your GitLab CI/CD pipeline. Ensure your pipeline is configured to include the stage where this job is defined.

4. **Monitoring**: Use GitLab's CI/CD pipeline view to monitor the progress of the substitution process. Check the job logs for detailed information about processed files.

## Troubleshooting
- **File Not Found Errors**:
    - Verify that the specified `DIR` or `FILES` paths are correct relative to the repository root.
    - Check if the files exist in the repository and are properly committed.

- **Substitution Failures**:
    - Ensure all required environment variables referenced in the template files are defined in your GitLab CI/CD settings.
    - Check for syntax errors in your template files.

- **Permission Issues**:
    - Verify that the GitLab runner has the necessary permissions to read from and write to the specified directories.

## Artifact Management
- Processed files are automatically saved as job artifacts.
- Artifacts include `.yaml`, `.yml`, `.env`, and `.txt` files (excluding `.gitlab-ci.yml`).
- Artifacts are set to expire after 1 week. Adjust the `expire_in` setting if needed.

## Notes
- Original `.tpl` files are preserved and not included in the artifacts.
- If `OUTPUT_DIR` is not specified, files are processed in-place (original directory).
- The script automatically handles both `.tpl` and non-`.tpl` input files.