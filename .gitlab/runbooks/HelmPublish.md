# Runbook

## Introduction
This runbook provides operational guidance for using the GitLab CI/CD pipeline to package and publish Helm charts. It outlines the steps necessary to prepare for execution, troubleshoot common issues, and manually rollback or delete published charts if needed.

## Pre-execution Checklist
- Verify that the Helm CLI is installed and configured correctly on the runner.
- Check that all Helm chart dependencies are defined and available.
= Ensure that your GitLab project has a tagged commit that specifies the chart version you wish to publish.

## Execution Steps
= Triggering the Pipeline: The pipeline is triggered automatically on pushing tags to the repository. Ensure that the tag matches the desired chart version.
- Monitoring: Use GitLab's CI/CD pipeline view to monitor the progress of the packaging and publishing stages.

## Troubleshooting
- Package Stage Fails: Check the output logs for errors related to Helm linting or dependency issues. Ensure all chart dependencies are correct and accessible.
- Publish Stage Fails: Verify that the CI_JOB_TOKEN has appropriate permissions and that the Helm repository is correctly specified and accessible.

## Rollback/Deletion
If you need to rollback or delete a published chart:

1. Access your Helm repository through the GitLab UI or API.
2. Locate the chart version you wish to remove.
3. Use the repository's management tools to delete or rollback the chart version.

## Support
If you encounter any issues while following this runbook, or if you have any questions or require support, please reach out to [devops@ccsolutions.io](mailto:devops@ccsolutions.io).