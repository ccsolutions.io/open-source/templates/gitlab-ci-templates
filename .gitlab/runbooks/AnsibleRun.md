# Runbook

## Introduction
This runbook provides operational guidance for using the GitLab CI/CD pipeline to run Ansible playbooks. It outlines the steps necessary to prepare for execution, troubleshoot common issues, and manually handle playbook execution or rollback if needed.

## Pre-execution Checklist
Ensure that the Ansible playbook, inventory file, and any required secrets files are available and up-to-date in the project repository.
Check that the Ansible Docker image specified in the pipeline is accessible to the CI/CD runner.
Verify that the vault password file for decrypting Ansible Vault secrets is correctly configured.

## Execution Steps
Triggering the Pipeline: The pipeline is triggered automatically on each push to the repository. Ensure that the .gitlab-ci.yml file includes the defined stages and tasks.

# Monitoring
Use GitLab's CI/CD pipeline view to monitor the progress of the playbook execution stages. Check the console output for any errors or warnings.

# Troubleshooting
- Check Stage Output: Examine the output logs of the pipeline stages for any error messages or warnings. Ensure that the playbook syntax is correct and that all required files and variables are properly configured.
- Ansible Errors: If the playbook fails during execution, review the Ansible error messages to identify the cause of the failure. Common issues include incorrect syntax, unreachable hosts, or missing dependencies.

# Rollback/Recovery
If you need to rollback changes or recover from playbook failures:

- Revert Changes: If the playbook introduced unwanted changes, revert the changes in the source code repository and commit the reversal.
- Rollback Playbook: If necessary, modify the playbook to revert the changes applied during the execution. Ensure that the playbook is tested thoroughly before re-running.
- Manual Intervention: In cases where manual intervention is required to fix issues, access the affected systems directly and apply necessary corrections following standard operating procedures.