# Terraform Pipeline Runbook

## Introduction

This runbook provides operational procedures for the Terraform CI/CD pipeline, including setup instructions, execution details, troubleshooting tips, and rollback procedures.

## Setup

Before executing the pipeline, ensure:

1. All software dependencies are installed on the runner (Terraform, Docker, SOPS).
2. The GitLab project is configured with the necessary environment variables (`GPG_KEY`, `PASSPHRASE`).
3. The encrypted variables file (`terraform.tfvars.encrypted`) is updated and available in the project repository.

## Execution

1. **Initiate the Pipeline**: The pipeline runs automatically upon pushing code changes to the repository.
2. **Monitor Progress**: Use GitLab's CI/CD interface to track the pipeline's progress across stages.

## Troubleshooting

- **Pre-commit Failures**: Check the output for specific linting errors and adjust the code accordingly.
- **Terraform Validate/Plan/Apply Errors**: Review Terraform's output logs for details on the failure. Common issues include syntax errors, missing resources, or misconfigured providers.

## Rollback

If a Terraform apply introduces an undesired state:

1. Use the Terraform CLI to inspect the current state and identify the changes to revert.
2. Prepare a new set of Terraform configurations that represent the desired state prior to the faulty apply.
3. Commit and push the rollback configurations to trigger the pipeline and restore the previous state.

## Additional Deployment Tasks

- For deployments involving Kubernetes, ensure `kubectl` is configured correctly with the appropriate context and access rights.
- Verify that any scripts or commands in the `deploy` stage are correct and aligned with the infrastructure's current state and requirements.

## Support
If you encounter any issues while following this runbook, or if you have any questions or require support, please reach out to [devops@ccsolutions.io](mailto:devops@ccsolutions.io).