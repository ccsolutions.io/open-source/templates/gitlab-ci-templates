# README.md

This repository contains different GitLab CI/CD pipeline configurations and examples.
Currently, there are pipelines for building Docker images with Kaniko, running Helm Lint on Helm charts, deployments with Helm and Helmfiles, a basic Terraform workflow with an additional Kubernetes-specific deployment step, and new OpenTofu pipelines for infrastructure management. These configurations are designed to provide a robust and flexible CI/CD pipeline for your projects.

## Contents

- [GitLab CI Kaniko Build Template](docs/DockerBuild-kaniko.md)
- [Helm Lint GitLab CI/CD Pipeline](docs/HelmLint.md)
- [Terraform GitLab CI/CD Pipeline](docs/TerraformPipeline.md)
- [Helm Deploy template](docs/DeployHelm.md)
- [Helmfile Deploy template](docs/DeployHelmfile.md)
- [Kubectl apply template](docs/KubectlApply.md)
- [Ansible Template](docs/AnsibleRun.md)
- [Deployment Pipeline](docs/DeploymentPipeline.md)
- [OpenTofu Destroy CI/CD Pipeline](docs/OpenTofuDestroy.md)
- [OpenTofu Test CI/CD Pipeline](docs/OpenTofuTest.md)
- [OpenTofu Deploy CI/CD Pipeline](docs/OpenTofu.md)
- [Environment Substitution Template](docs/EnvSubs.md)

## GitLab CI Kaniko Build Template

This is a fork of the GitLab CI YAML template for building Docker images with Kaniko, originally developed by Syseleven GmbH. This forked version contains modifications and enhancements developed by CCSolutions.io UG. 

## Import the pipeline
For use this pipeline as a job in another pipeline, copy this code into your .gilab-ci.yml
```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'DockerBuild-kaniko.yml'
    ref: 'v2.0.5'

kaniko-custom-build:
  extends: .kaniko-build-base
  variables:
    DOCKERFILE: custom.Dockerfile
    IMAGE_TAG: "custom"

```

## Variables:
For run this job in your pipeline, its need to set this variables

- `CI_REGISTRY`: The URL of your Docker registry.
- `CI_REGISTRY_USER`: The username to authenticate with your Docker registry.
- `CI_REGISTRY_PASSWORD`: The password to authenticate with your Docker registry.
- `CI_PROJECT_DIR`: The absolute path to your project directory in the GitLab Runner environment.
- `CI_COMMIT_REF_SLUG`: The branch or tag name for which project is built.
- `CI_COMMIT_TAG`: The commit tag name. Present only when building tags.
- `CI_PIPELINE_SOURCE`: Indicates how the pipeline was triggered.
- `CI_DEFAULT_BRANCH`: The default branch for your project.

You can set these variables in a Variable section for the job in the project settings
[Read more](docs/DockerBuild-kaniko.md)

## Helm Lint GitLab CI/CD Pipeline

Contains a GitLab CI/CD pipeline configuration (`gitlab-ci.yaml`) that runs Helm Lint on your Helm charts. Helm Lint is a tool that checks for common issues in your Helm charts and provides suggestions for improvements.

## Import the pipeline
This job used `alpine/helm:3.13.3` docker image, in this case the job [Build Kaniko template](docs/DockerBuild-kaniko.md) is part of this pipeline
To importing this pipeline to another CI/CD pipeline, you need to copy the next code for the job in the pipeline

```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'HelmLint.yml'
    ref: 'v2.0.5'

helm-lint:
  stage: lint 
  extends: .helm-lint
```
***Check the tag version in case of updates***
## Variables

- `GIT_DEPTH`: The number of commits to fetch from the Git repository. Set to `"1"` to fetch only the latest commit.
- `HELM_VERSION`: The version of Helm to use. Set to `latest` to use the latest version.
- `HELM_PATH`: The path to the Helm charts to lint. Leave blank to lint all charts in the repository.
- `HELM_ADDITIONAL_ARGS`: Any additional arguments to pass to the `helm lint` command.
- `HELM_LINT_STRICT`: Set to `'--strict'` to enable strict linting, which treats warnings as errors.

## Stages:
- `helm lint`: Checks if your chart directory has the necessary files, such as Chart.yaml, values.yaml, and templates directory, also checks the chart files looking for for errors or inconsistencies and sintax errors.

## Conditions:
This pipeline only is trigger if there`s a merge request and if the Commit branch is the default branch
 
 [Read more](docs/HelmLint.md)

## Terraform GitLab CI/CD Pipeline

Contains basic Terraform steps and a Kubernetes-specific deployment step. Terraform is an infrastructure as code (IaC) tool used in IT to manage and provision infrastructure. 

## Stages:
- `init`: This initial stage could involve things like downloading dependencies, installing tools, or configuring the build agent.
- `pre-commit`: This stage can be used to perform checks on code before it's even committed to the version control system. 
- `validate`: This stage typically involves validating the code itself. This could include tasks like syntax checking or code formatting checks
- `plan`: This stage might be used to create a plan of what changes the terraform in the pipeline will make.
- `build`: This is the stage where terraform built ,compiling code, packaging it into a deployable artifact and running any necessary build tests.
- `deploy`: This stage is where the artifact is deployed.


[Read more](docs/TerraformPipeline.md)

## Helm Deploy template 

This pipeline template is designed for deploying Helm charts to a Kubernetes cluster. Helm charts are a collection of YAML files used to describe a set of Kubernetes resources, making them easy to manage and deploy.

### Import the pipeline
For using and running the template, add this code to the CI/CD pipeline 
```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'HelmPipelines/DeployHelm.yml'
    ref: 'v2.0.5'

deploy-helm:
  stage: deploy
  extends: .deploy-helm
  variables:

```
***Check the tag version in case of updates***
For running this pipeline you must fill the variables values. 

### Variables
- `ARGS:` Its hold any additional arguments that we need yo pass to the `helm upgrade --install`command like image-repository, image-tag and credentials.registry
- `KUBE_CONTEXT:` The Kubernetes context where the Helm charts will be deployed. 
- `RELEASE_NAME:` The name will be assigned to the deployed helm release
- `REPO_NAME:` The alias for the repository URL 
- `NAMESPACE:` The namespace for the kubernetes cluster where the chart is deploying  
- `REPO:` The URL repo por the helm repository where the chart is.

You can set these variables in a Variable section for the job in the project settings

### Stages
- `deploy-helm`: This stage will deploy a helm chart, the commands that are running in this stage are: 
- `helm repo add` will add the repository of the chart that we want to Deploy  
- `helm repo update` will update or refresh the chart info from the chart repository
- `helm upgrade --install` will install and deploy the helm chart in the case that there is not an existing release name.

[Read more](docs/DeployHelm.md)

## Helmfile Deploy template

This GitLab CI/CD pipeline template is designed for deploying Helm charts using Helmfile to a Kubernetes cluster. Helmfile is a declarative specification for deploying Helm charts, making it easier to manage complex Helm deployments. The pipeline includes stages for: 

### Import the project
For using and running the template, add this code to the CI/CD pipeline 
```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'HelmPipelines/HelmPublish.yml'
    ref: 'v2.0.5'

lint-helmfile:
  stage: lint
  extends: .lint-helmfile

diff-helmfile:
  stage: diff
  extends: .diff-helmfile

deploy-helmfile:
  stage: deploy
  extends: .deploy-helmfile

```
***Check the tag version in case of updates***
This will import this template to your helmfile deployment.


### Variables 
- `KUBE_CONTEXT`: The Kubernetes context where the Helm charts will be deployed.
- `ENVIROMENT`: The target enviroment for the helmfile deployment.

For getting started we have to set the variables.
You can set these variables in a Variable section for the job in the proyect settings

### Stages:

- `lint-helmfile`: Run a Helm lint across the helmfile, running a series of tests to verify that the chart is well-formed.

- `diff-helmfile`: Run a Helm diff in the upgraded helmfile, showing the differences with the previous version.

- `deploy-helmfile: Run a Helm apply. if there`s any change between the actual version and the previous version (helm diff), this changes will sync with the Kubernetes cluster as described in the helmfile.

[Read more](docs/DeployHelmfile.md)

## Ansible Template

This GitLab CI/CD pipeline template is designed for running Ansible playbooks. Ansible is a powerful IT automation tool that can configure systems, deploy software, and orchestrate more advanced IT tasks. This pipeline includes two stages: one for checking the playbook syntax and another for executing the playbook.

### Import the project
For using and running the template, add this code to the CI/CD pipeline 
```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'ansibleRun/AnsibleRun.yml'
    ref: 'v2.0.5'

ansible-check:
  stage: check
  extends: .ansible_check
  variables:

ansible-run:
  stage: run
  extends: .ansible_run
  variables:
```
***Check the tag version in case of updates***
This will import this template to your helmfile deployment.


### Variables 
- `ANSIBLE_DIR`: The directory where the Ansible files are located. Default is ".".
- `PLAYBOOK`: The name of the playbook file to run. Default is "playbook.yaml".
- `INVENTORY`: The name of the inventory file. Default is "inventory.yaml".
- `SECRETS`: The name of the secrets file. Default is "secrets.yaml".
- `VAULT_PASSWORD_FILE`: The path to the vault password file. Default is ".vault.secret".
- `ANSIBLE_VERSION`: The version of the Ansible Docker image to use. Default is "2.15-alpine-3.18".

For getting started we have to set the variables.
You can set these variables in a Variable section for the job in the project settings

### Stages:

- `check`: This stage checks the syntax and potential changes of the Ansible playbook without applying them. It uses the --check flag to perform a dry run.

- `run`: This stage runs the Ansible playbook to apply the desired configurations to the target systems.

[Read more](docs/AnsibleRun.md)

## Deployment Pipeline

This GitLab CI/CD pipeline template is designed for deploying applications to a Kubernetes cluster using Helm charts. The pipeline includes stages for building the Docker image, deploying the Helm chart, and deleting the Kubernetes namespace.

### Import the pipeline

For using and running the template, add this code to the CI/CD pipeline 
```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'DeploymentPipeline/DeploymentPipeline.yml'
    ref: 'v2.0.5'
    
build-image:
  stage: build
  extends: .build-image
  variables:
    IMAGE_TAG: "latest"

deploy:
  stage: deploy
  extends: .deploy-helm
  variables:
    IMAGE_TAG: "latest"
    NAMESPACE: "default"
    KUBE_CONTEXT: "default"
    RELEASE_NAME: "my-release"
    APPLICATION_NAME: "my-app"
    CHART_NAME: "my-chart"
    REPO_URL: "https://charts.example.com"
    REPO_NAME: "example-charts"
    HOST_URL: "my-app.example.com"
    SECRET_NAME: "my-secret"
    ARGS: "--set image.tag=latest"

delete:
  stage: delete
  extends: .delete-environment
  variables:
      KUBE_CONTEXT: "default"
```

***Check the tag version in case of updates***

This will import this template to your helmfile deployment.

### Variables

The necessary variable are the same as the [DockerBuild-Kaniko](docs/DockerBuild-kaniko.md) and [DeployHelm](docs/DeployHelmfile.md) templates.

With the addition of the following variables:

- `HOST_URL`: The URL of the application used by the Ingress.
- `SECRET_NAME`: Secret Name for the TLS certificate.

### Pipeline Stages

- `Build`: This stage runs the Docker build process using Kaniko, building the Docker image for the application.
- `Deploy`: This stage aims to Deploy the Helm charts to the Kubernetes cluster using the specified configuration.
- `Delete`: This stage aims to delete the namespace from the Kubernetes cluster.

[Read more](docs/DeploymentPipeline.md)

## OpenTofu Destroy Pipeline

This pipeline is designed for safely destroying infrastructure managed by OpenTofu. It includes safety checks and a single destroy stage.

### Import the pipeline
To use this pipeline as a job in another pipeline, copy this code into your .gitlab-ci.yml:

```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'OpenTofuPipelines/OpenTofuDestroy.yml'
    ref: 'v2.0.5'

opentofu-destroy:
  extends: .tofu:destroy
```

### Variables
For running this job in your pipeline, you need to set these variables:

- `GPG_KEY`: The GPG key for decrypting sensitive files.
- `PASSPHRASE`: The passphrase for the GPG key.
- `TOFU_VARS_ENCRYPTED`: The path to the encrypted OpenTofu variables file.
- `TOFU_VARS`: The path to the decrypted OpenTofu variables file.

You can set these variables in the Variable section for the job in the project settings.

### Stages
- `destroy`: This stage runs the OpenTofu destroy command to remove all managed infrastructure.

[Read more](docs/OpenTofuDestroy.md)

## OpenTofu Main Pipeline

This pipeline provides a complete workflow for managing infrastructure with OpenTofu, including initialization, planning, and applying changes.

### Import the pipeline
To use this pipeline, add the following to your .gitlab-ci.yml:

```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'OpenTofuPipelines/OpenTofu.yml'
    ref: 'v2.0.5'

opentofu-pre-commit:
  extends: .tofu:pre-commit

opentofu-init:
  extends: .tofu:init

opentofu-plan:
  extends: .tofu:plan

opentofu-apply:
  extends: .tofu:apply
```

### Variables
Required variables include:

- `GPG_KEY`: The GPG key for decrypting sensitive files.
- `PASSPHRASE`: The passphrase for the GPG key.
- `TF_ADDRESS`: The OpenTofu state management API address.
- `TOFU_VARS_ENCRYPTED`: The path to the encrypted OpenTofu variables file.
- `TOFU_VARS`: The path to the decrypted OpenTofu variables file.

### Stages
- `pre-commit`: Runs various pre-commit hooks for linting and ensuring code quality.
- `init`: Initializes the OpenTofu working directory.
- `plan`: Generates and outputs an execution plan.
- `apply`: Applies the changes required to reach the desired state of the infrastructure.

[Read more](docs/OpenTofu.md)

## OpenTofu Test Pipeline

This pipeline focuses on running pre-commit checks and tests for OpenTofu configurations.

### Import the pipeline
Add the following to your .gitlab-ci.yml to use this pipeline:

```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'OpenTofuPipelines/OpenTofuTest.yml'
    ref: 'v2.0.5'

opentofu-pre-commit:
  extends: .tofu:pre-commit

opentofu-test:
  extends: .tofu:test
```

### Stages
- `pre-commit`: Runs various pre-commit hooks for linting and ensuring code quality.
- `test`: Executes OpenTofu tests on the codebase.

[Read more](docs/OpenTofuTest.md)

## Environment Substitution Template

This GitLab CI/CD pipeline template is designed for performing environment variable substitution in template files. It's particularly useful for dynamically generating configuration files based on environment variables, making it easier to manage different environments and configurations in your CI/CD pipeline.

### Import the pipeline
To use this pipeline as a job in another pipeline, copy this code into your .gitlab-ci.yml:

```yaml
include:
  - project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'utils/EnvSubs.yml'
    ref: 'v2.0.5'

env-substitution-job:
  extends: .env-substitution
  variables:
    DIR: "path/to/template/directory"
    FILES: "config1.yaml.tpl,config2.yml.tpl"
    OUTPUT_DIR: "path/to/output/directory"
```

### Variables
For running this job in your pipeline, you need to set these variables:

- `DIR`: The directory containing `.tpl` files to be processed. Optional.
- `FILES`: A comma-separated list of specific files to process. Optional.
- `OUTPUT_DIR`: The directory where processed files will be saved. If not specified, files are processed in-place.

You can set these variables in the Variable section for the job in the project settings.

### Stages
- `substitution`: This stage performs environment variable substitution on the specified template files. It uses the `envsubst` command from the `gettext` package to replace placeholders in the template files with actual environment variable values.

### Template Behavior
- Processes all `.tpl` files in the specified `DIR` if provided.
- If `FILES` is specified, processes only those files (they don't need to have a `.tpl` extension).
- Removes the `.tpl` extension from processed files (if present).
- Saves processed files in `OUTPUT_DIR` if specified, otherwise replaces the original files.
- Preserves original `.tpl` files.

### Artifacts
- Processed files (`.yaml`, `.yml`, `.env`, `.txt`) are automatically saved as job artifacts.
- Artifacts expire after 1 week by default.
- The `.gitlab-ci.yml` file is excluded from artifacts.

[Read more](docs/EnvSubs.md)

## Kubectl apply template 

This GitLab CI/CD pipeline template is designed for deploying kubernetes Resources using kubectl CLI in a Kubernetes cluster. 

## Requirements
A GitLab account and a project repository.
A Kubernetes cluster where the Resources will be deployed.
The kubectl CLI installed on the runner used by the CI/CD pipeline.

## Variables
- `KUBE_CONTEXT`: The Kubernetes context where the Helm charts will be deployed.
- `RESOURCE_PATH`: Path to the kubernetes resources to deploy
- `NAMESPACE`: Namespace in which the kubernetes resourse is deployed
- `KUBE_OPT`: For other CLI options, example: --dry-run


## How to Use
Add this CI/CD template to your `.gitlab-ci.yml` file in your GitLab repository.
Ensure that the `KUBE_CONTEXT` variable in the template matches your Kubernetes cluster's context.
Push your changes to GitLab, and the pipeline will automatically apply the kubernetes file into the cluster

## Example
```yaml
---
- project: 'ccsolutions.io/open-source/templates/gitlab-ci-templates'
    file: 'HelmPipelines/KubectlApply.yml'
    ref: '2.0.5'  


deploy:kubectl-apply:
  extends: .kubectl-apply

---
```
[Read more](docs/KubectlApply.md)

## Support

For support or any questions, please reach out to us at [devops@ccsolutions.io](mailto:devops@ccsolutions.io).

## License

This project is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT).

## Authors

This project was created by [CCSolutions.io UG](mailto:devops@ccsolutions.io).
